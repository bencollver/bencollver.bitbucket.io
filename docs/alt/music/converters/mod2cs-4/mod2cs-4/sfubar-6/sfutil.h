#ifndef __SFUTIL_H__
#define __SFUTIL_H__
#include <riffraff/rifftypes.h>
extern void sf_to_txt(char *, char *, int);
extern void txt_to_sf(char *, char *);
#define SFUTIL_VERSION 6

#define SHMIN -32768
#define SHOOBVAL -32769

typedef enum {
  file_type_wav,
  file_type_aif,
  file_type_end
} file_type_t;

typedef struct {
  myBYTE byLo;
  myBYTE byHi;
} rangesType;

typedef union {
  rangesType ranges;
  mySHORT shAmount;
  myWORD wAmount;
} genAmountType;

typedef enum {
  sfg_startAddrsOffset = 0,
  sfg_endAddrsOffset,
  sfg_startloopAddrsOffset,
  sfg_endloopAddrsOffset,
  sfg_startAddrsCoarseOffset,
  sfg_modLfoToPitch,
  sfg_vibLfoToPitch,
  sfg_modEnvToPitch,
  sfg_initialFilterFc,
  sfg_initialFilterQ,
  sfg_modLfoToFilterFc,
  sfg_modEnvToFilterFc,
  sfg_endAddrsCoarseOffset,
  sfg_modLfoToVolume,
  sfg_unused1,
  sfg_chorusEffectsSend,
  sfg_reverbEffectsSend,
  sfg_pan,
  sfg_unused2,
  sfg_unused3,
  sfg_unused4,
  sfg_delayModLFO,
  sfg_freqModLFO,
  sfg_delayVibLFO,
  sfg_freqVibLFO,
  sfg_delayModEnv,
  sfg_attackModEnv,
  sfg_holdModEnv,
  sfg_decayModEnv,
  sfg_sustainModEnv,
  sfg_releaseModEnv,
  sfg_keynumToModEnvHold,
  sfg_keynumToModEnvDecay,
  sfg_delayVolEnv,
  sfg_attackVolEnv,
  sfg_holdVolEnv,
  sfg_decayVolEnv,
  sfg_sustainVolEnv,
  sfg_releaseVolEnv,
  sfg_keynumToVolEnvHold,
  sfg_keynumToVolEnvDecay,
  sfg_instrument,
  sfg_reserved1,
  sfg_keyRange,
  sfg_velRange,
  sfg_startloopAddrsCoarseOffset,
  sfg_keynum,
  sfg_velocity,
  sfg_initialAttenuation,
  sfg_reserved2,
  sfg_endloopAddrsCoarseOffset,
  sfg_coarseTune,
  sfg_fineTune,
  sfg_sampleID,
  sfg_sampleModes,
  sfg_reserved3,
  sfg_scaleTuning,
  sfg_exclusiveClass,
  sfg_overridingRootKey,
  sfg_unused5,
  sfg_endOper
} sf_gen_enum_t;

typedef enum {
  sml_loop_none = 0,
  sml_loop_continuous,
  sml_loop_unused,
  sml_loop_release
} sample_modes_enum_t;

typedef enum {
  zt_instrument = 0,
  zt_preset
} zone_type_enum_t;

typedef struct {
  zone_type_enum_t zone_type;
  char *keyn;
  char *key;
  char *itemn;
  char *item;
  int item_max;
  int oper;
  char *bagc;
  char *genc;
  char *modc;
  char *hdrc;
} zone_type_t;
#endif
