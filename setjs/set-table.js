// make global variables explicit
global = (function () {
    return this;
}).call(null);

// use global.classes as a "namespace"
if (typeof global.classes == "undefined") {
    global.classes = {};
}

global.classes.card = function (num, fill, hue, shape) {
    this.clear = function () {
        this.sel = false;
    };

    this.compareProp = function (c2, c3, name) {
        var c1 = this,
            key,
            retval = [],
            uniq = {};
        uniq[c1.getProp(name)] = 1;
        uniq[c2.getProp(name)] = 1;
        uniq[c3.getProp(name)] = 1;
        for (key in uniq) {
            retval.push(key);
        }
        return retval;
    };

    this.getName = function () {
        var retval = this.num + this.fill + this.hue + this.shape;
        return retval;
    };

    this.getProp = function (name) {
        var retval = this[name];
        return retval;
    };

    this.isSelected = function () {
        var retval = this.sel;
        return retval;
    };

    this.select = function () {
        this.sel = true;
    };

    this.validateProp = function (c2, c3, name) {
        var len,
            retval,
            uniq;
        uniq = this.compareProp(c2, c3, name);
        len = uniq.length;
        retval = (len == 1 || len == 3);
        return retval;
    };

    this.validateSet = function (c2, c3) {
        var c1 = this,
            retval;
        retval = (
            !c1.validateProp(c2, c3, "fill")  ? "fill"  :
            !c1.validateProp(c2, c3, "hue")   ? "hue"   :
            !c1.validateProp(c2, c3, "num")   ? "num"   :
            !c1.validateProp(c2, c3, "shape") ? "shape" :
                                                "valid"
        );
        return retval;
    };

    this.num = num;
    this.fill = fill;
    this.hue = hue;
    this.shape = shape;
    this.clear();
}

global.classes.deck = function () {
    var f,
        fills,
        h,
        hues,
        n,
        nums,
        s,
        shapes;

    this.deal = function () {
        var retval = this.cards.shift();
        return retval;
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.shuffle = function () {
        var i,
            index,
            temp;
        for (i = 0; i < this.cards.length; i++) {
            index = parseInt(Math.floor(Math.random() *
                (this.cards.length - 1)), 10);
            temp = this.cards[i];
            this.cards[i] = this.cards[index];
            this.cards[index] = temp;
        }
    };

    this.cards = [];

    // create cards
    nums = ["One", "Two", "Three"];
    fills = ["Hollow", "Shaded", "Solid"];
    hues = ["Blue", "Green", "Red"];
    shapes = ["Circle", "Square", "Triangle"];
    for (n = 0; n < nums.length; n++) {
        for (f = 0; f < fills.length; f++) {
            for (h = 0; h < hues.length; h++) {
                for (s = 0; s < shapes.length; s++) {
                    this.cards.push(new global.classes.card(
                        nums[n],
                        fills[f],
                        hues[h],
                        shapes[s]
                    ));
                }
            }
        }
    }

    this.shuffle();
}

global.classes.table = function () {
    var i;

    this.clearSelection = function () {
        var card,
            i;
        for (i = 0; i < this.cards.length; i++) {
            card = this.cards[i];
            if (card.isSelected()) {
                card.clear();
                global.ui.drawCard(i, card);
            }
        }
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.getSelection = function () {
        var i,
            retval = [];
        for (i = 0; i < this.cards.length; i++) {
            if (this.cards[i].isSelected()) {
                retval.push(this.cards[i]);
            }
        }
        return retval;
    };

    this.getWasteLength = function () {
        var retval = this.waste.length;
        return retval;
    };

    this.hasSet = function () {
        var a,
            b,
            c,
            c1,
            c2,
            c3;
        for (a = 0; a < this.cards.length; a++) {
            c1 = this.cards[a];
            for (b = a + 1; b < this.cards.length; b++) {
                c2 = this.cards[b];
                for (c = b + 1; c < this.cards.length; c++) {
                    c3 = this.cards[c];
                    if (c1.validateSet(c2, c3) == "valid") {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    this.more = function () {
        var i;
        if (this.cards.length > this.min) {
            global.ui.print("Enough cards already.");
        } else if (global.deck.getLength() === 0) {
            global.ui.print("No more cards.");
        } else if (this.hasSet()) {
            global.ui.print("Set exists.");
        } else {
            for (i = 0; i < 3; i++) {
                if (global.deck.getLength() > 0) {
                    this.cards.push(global.deck.deal());
                }
            }
            this.refresh();
        }
    };

    this.refresh = function () {
        var i;
        for (i = 0; i < this.spots; i++) {
            if (i < this.cards.length) {
                global.ui.drawCard(i, this.cards[i]);
            } else {
                global.ui.drawCard(i, null);
            }
        }
        global.ui.score();
    };

    this.replaceSelection = function () {
        var i;
        for (i = 0; i < this.cards.length; i++) {
            while (typeof this.cards[i] != "undefined" &&
                this.cards[i].isSelected()
            ) {
                this.waste.push(this.cards[i]);
                if (this.cards.length <= this.min &&
                    global.deck.getLength() > 0
                ) {
                    this.cards[i] = global.deck.deal();
                } else {
                    this.cards.splice(i, 1);
                }
            }
        }
    };

    this.select = function (index) {
        var card,
            c1,
            c2,
            c3,
            result = "ok";
        if (index < this.cards.length) {
            card = this.cards[index];
            card.select();
            global.ui.drawCard(index, card);
            var sel = this.getSelection();
            if (sel.length == 3) {
                c1 = sel[0];
                c2 = sel[1];
                c3 = sel[2];
                result = c1.validateSet(c2, c3);
            }
        }

        switch (result) {
        case "ok":
            // incomplete set, continue
            break;
        case "valid":
            global.ui.print("Correct, +1 point");
            global.points++;
            this.replaceSelection();
            this.refresh();
            break;
        default:
            global.ui.print("Wrong " + result + ", -1 point");
            global.points--;
            this.clearSelection();
            this.refresh();
            break;
        }
    };

    this.cards = [];
    this.min = 12;
    this.spots = 15;
    this.waste = [];
    for (i = 0; i < this.min; i++) {
        this.cards.push(global.deck.deal());
    }
}

global.classes.ui = function () {
    this.drawCard = function (spot, card) {
        document.getElementById("c" + spot).className = (card === null ?
            "blank" :
            card.getName() + (card.isSelected() ?
                " selected" :
                ""
            )
        );
    };

    this.print = function (str) {
        alert(str);
    };

    this.score = function () {
        var el;

        el = document.getElementById("points");
        el.innerHTML = global.points;

        el = document.getElementById("deck");
        el.innerHTML = global.deck.getLength();

        el = document.getElementById("waste");
        el.innerHTML = global.table.getWasteLength();
    };

    // use custom CSS for Firefox
    if (typeof navigator == "object" &&
        navigator.userAgent.search(/firefox/i) > -1
    ) {
        var head = document.getElementsByTagName("head")[0],
            link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = "firefox-table.css";
        head.appendChild(link);
    }
}

function main () {
    global.deck = new global.classes.deck();
    global.points = 0;
    global.table = new global.classes.table();
    global.ui = new global.classes.ui();

    global.table.refresh();
}
