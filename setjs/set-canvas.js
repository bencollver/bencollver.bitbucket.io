// make global variables explicit
global = (function () {
    return this;
}).call(null);

// use global.classes as a "namespace"
if (typeof global.classes == "undefined") {
    global.classes = {};
}

global.classes.card = function (num, fill, hue, shape) {
    this.clear = function () {
        this.sel = false;
    };

    this.compareProp = function (c2, c3, name) {
        var c1 = this,
            key,
            retval = [],
            uniq = {};
        uniq[c1.getProp(name)] = 1;
        uniq[c2.getProp(name)] = 1;
        uniq[c3.getProp(name)] = 1;
        for (key in uniq) {
            retval.push(key);
        }
        return retval;
    };

    this.getName = function () {
        var retval = this.num + this.fill + this.hue + this.shape;
        return retval;
    };

    this.getProp = function (name) {
        var retval = this[name];
        return retval;
    };

    this.isSelected = function () {
        var retval = this.sel;
        return retval;
    };

    this.select = function () {
        this.sel = true;
    };

    this.validateProp = function (c2, c3, name) {
        var len,
            retval,
            uniq;
        uniq = this.compareProp(c2, c3, name);
        len = uniq.length;
        retval = (len == 1 || len == 3);
        return retval;
    };

    this.validateSet = function (c2, c3) {
        var c1 = this,
            retval;
        retval = (
            !c1.validateProp(c2, c3, "fill")  ? "fill"  :
            !c1.validateProp(c2, c3, "hue")   ? "hue"   :
            !c1.validateProp(c2, c3, "num")   ? "num"   :
            !c1.validateProp(c2, c3, "shape") ? "shape" :
                                                "valid"
        );
        return retval;
    };

    this.num = num;
    this.fill = fill;
    this.hue = hue;
    this.shape = shape;
    this.clear();
}

global.classes.deck = function () {
    var f,
        fills,
        h,
        hues,
        n,
        nums,
        s,
        shapes;

    this.deal = function () {
        var retval = this.cards.shift();
        return retval;
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.shuffle = function () {
        var i,
            index,
            temp;
        for (i = 0; i < this.cards.length; i++) {
            index = parseInt(Math.floor(Math.random() *
                (this.cards.length - 1)), 10);
            temp = this.cards[i];
            this.cards[i] = this.cards[index];
            this.cards[index] = temp;
        }
    };

    this.cards = [];

    // create cards
    nums = ["One", "Two", "Three"];
    fills = ["Hollow", "Shaded", "Solid"];
    hues = ["Blue", "Green", "Red"];
    shapes = ["Circle", "Square", "Triangle"];
    for (n = 0; n < nums.length; n++) {
        for (f = 0; f < fills.length; f++) {
            for (h = 0; h < hues.length; h++) {
                for (s = 0; s < shapes.length; s++) {
                    this.cards.push(new global.classes.card(
                        nums[n],
                        fills[f],
                        hues[h],
                        shapes[s]
                    ));
                }
            }
        }
    }

    this.shuffle();
}

global.classes.table = function () {
    var i;

    this.clearSelection = function () {
        var card,
            i;
        for (i = 0; i < this.cards.length; i++) {
            card = this.cards[i];
            if (card.isSelected()) {
                card.clear();
                global.ui.drawCard(i, card);
            }
        }
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.getSelection = function () {
        var i,
            retval = [];
        for (i = 0; i < this.cards.length; i++) {
            if (this.cards[i].isSelected()) {
                retval.push(this.cards[i]);
            }
        }
        return retval;
    };

    this.getWasteLength = function () {
        var retval = this.waste.length;
        return retval;
    };

    this.hasSet = function () {
        var a,
            b,
            c,
            c1,
            c2,
            c3;
        for (a = 0; a < this.cards.length; a++) {
            c1 = this.cards[a];
            for (b = a + 1; b < this.cards.length; b++) {
                c2 = this.cards[b];
                for (c = b + 1; c < this.cards.length; c++) {
                    c3 = this.cards[c];
                    if (c1.validateSet(c2, c3) == "valid") {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    this.more = function () {
        var i;
        if (this.cards.length > this.min) {
            global.ui.print("Enough cards already.");
        } else if (global.deck.getLength() === 0) {
            global.ui.print("No more cards.");
        } else if (this.hasSet()) {
            global.ui.print("Set exists.");
        } else {
            for (i = 0; i < 3; i++) {
                if (global.deck.getLength() > 0) {
                    this.cards.push(global.deck.deal());
                }
            }
            this.refresh();
        }
    };

    this.refresh = function () {
        var i;
        for (i = 0; i < this.spots; i++) {
            if (i < this.cards.length) {
                global.ui.drawCard(i, this.cards[i]);
            } else {
                global.ui.drawCard(i, null);
            }
        }
        global.ui.score();
    };

    this.replaceSelection = function () {
        var i;
        for (i = 0; i < this.cards.length; i++) {
            while (typeof this.cards[i] != "undefined" &&
                this.cards[i].isSelected()
            ) {
                this.waste.push(this.cards[i]);
                if (this.cards.length <= this.min &&
                    global.deck.getLength() > 0
                ) {
                    this.cards[i] = global.deck.deal();
                } else {
                    this.cards.splice(i, 1);
                }
            }
        }
    };

    this.select = function (index) {
        var card,
            c1,
            c2,
            c3,
            result = "ok";
        if (index < this.cards.length) {
            card = this.cards[index];
            card.select();
            global.ui.drawCard(index, card);
            var sel = this.getSelection();
            if (sel.length == 3) {
                c1 = sel[0];
                c2 = sel[1];
                c3 = sel[2];
                result = c1.validateSet(c2, c3);
            }
        }

        switch (result) {
        case "ok":
            // incomplete set, continue
            break;
        case "valid":
            global.ui.print("Correct, +1 point");
            global.points++;
            this.replaceSelection();
            this.refresh();
            break;
        default:
            global.ui.print("Wrong " + result + ", -1 point");
            global.points--;
            this.clearSelection();
            this.refresh();
            break;
    }
    };

    this.cards = [];
    this.min = 12;
    this.spots = 15;
    this.waste = [];
    for (i = 0; i < this.min; i++) {
        this.cards.push(global.deck.deal());
    }
}

global.classes.ui = function () {
    var i,
        key;

    this.drawCard = function (spot, card) {
        var cutout,
            location = this.locations[spot],
            name,
            src;
        this.ctx.strokeStyle = "white";
        this.ctx.fillRect(location.x, location.y, this.width, this.height);
        if (card !== null) {
            name = card.getName();
            cutout = this.cutouts[name];
            src = this["cards" + cutout.index];
            this.ctx.drawImage(
                src,
                cutout.sx,
                cutout.sy,
                this.widthCard,
                this.heightCard,
                location.x + this.border + this.pad,
                location.y + this.border + this.pad,
                this.widthCard,
                this.heightCard
           );
           if (card.isSelected()) {
               this.ctx.strokeStyle = "purple";
               this.ctx.strokeRect(
                   location.x + this.pad + this.border,
                   location.y + this.pad + this.border,
                   this.widthCard + this.pad,
                   this.heightCard + this.pad
               );
           }
        }
    };

    this.click = function (event) {
        var e,
            i,
            l,
            x,
            y;
        e = window.event ?
            window.event :
            event;
        x = e.clientX;
        y = e.clientY;
        for (i = 0; i < 15; i++) {
            l = this.locations[i];
            if (x > l.x &&
                x < (l.x + this.width) &&
                y > l.y &&
                y < (l.y + this.height)
            ) {
                table.select(i);
                break;
            }
        }
    };

    this.print = function (str) {
        alert(str);
    };

    this.score = function () {
        var el;

        el = document.getElementById("points");
        el.innerHTML = global.points;

        el = document.getElementById("deck");
        el.innerHTML = global.deck.getLength();

        el = document.getElementById("waste");
        el.innerHTML = global.table.getWasteLength();
    };

    this.border = 5;
    this.pad = 1;
    this.heightCard = 95;
    this.widthCard = 152;
    this.canvas = document.getElementById("board");
    if (typeof this.canvas.getContext == "undefined") {
        throw Error("No browser support for canvas elements.");
    }
    this.ctx = this.canvas.getContext("2d");
    this.ctx.fillStyle = "white";
    this.ctx.lineCap = "round";
    this.ctx.lineJoin = "round";
    this.ctx.lineWidth = this.border;
    this.height = this.heightCard + 2 * this.border + 2 * this.pad;
    this.width = this.widthCard + 2 * this.border + 2 * this.pad;
    this.locations = {
        "0":  {x: 0 * this.width, y: 0 * this.height},
        "1":  {x: 1 * this.width, y: 0 * this.height},
        "2":  {x: 2 * this.width, y: 0 * this.height},
        "3":  {x: 3 * this.width, y: 0 * this.height},
        "12": {x: 4 * this.width, y: 0 * this.height},
        "4":  {x: 0 * this.width, y: 1 * this.height},
        "5":  {x: 1 * this.width, y: 1 * this.height},
        "6":  {x: 2 * this.width, y: 1 * this.height},
        "7":  {x: 3 * this.width, y: 1 * this.height},
        "13": {x: 4 * this.width, y: 1 * this.height},
        "8":  {x: 0 * this.width, y: 2 * this.height},
        "9":  {x: 1 * this.width, y: 2 * this.height},
        "10": {x: 2 * this.width, y: 2 * this.height},
        "11": {x: 3 * this.width, y: 2 * this.height},
        "14": {x: 4 * this.width, y: 2 * this.height}
    };
    this.cutouts = {
        OneHollowRedCircle:       {index: 1, sx:   0, sy:   0},
        OneHollowGreenCircle:     {index: 1, sx: 151, sy:   0},
        OneHollowBlueCircle:      {index: 1, sx: 303, sy:   0},
        OneSolidRedSquare:        {index: 1, sx:   0, sy:  95},
        OneSolidGreenSquare:      {index: 1, sx: 151, sy:  95},
        OneSolidBlueSquare:       {index: 1, sx: 302, sy:  95},
        OneShadedRedSquare:       {index: 1, sx:   0, sy: 189},
        OneShadedGreenSquare:     {index: 1, sx: 152, sy: 189},
        OneShadedBlueSquare:      {index: 1, sx: 302, sy: 189},
        OneHollowRedSquare:       {index: 1, sx:   0, sy: 284},
        OneHollowGreenSquare:     {index: 1, sx: 151, sy: 284},
        OneHollowBlueSquare:      {index: 1, sx: 302, sy: 284},
        OneSolidRedTriangle:      {index: 1, sx:   0, sy: 379},
        OneSolidGreenTriangle:    {index: 1, sx: 151, sy: 379},
        OneSolidBlueTriangle:     {index: 1, sx: 302, sy: 379},
        OneShadedRedTriangle:     {index: 1, sx:   0, sy: 473},
        OneShadedGreenTriangle:   {index: 1, sx: 151, sy: 473},
        OneShadedBlueTriangle:    {index: 1, sx: 303, sy: 473},
        OneHollowRedTriangle:     {index: 1, sx:   0, sy: 568},
        OneHollowGreenTriangle:   {index: 1, sx: 151, sy: 568},
        OneHollowBlueTriangle:    {index: 1, sx: 302, sy: 568},
        TwoShadedRedSquare:       {index: 2, sx:   0, sy:   0},
        TwoShadedGreenSquare:     {index: 2, sx: 151, sy:   0},
        TwoShadedBlueSquare:      {index: 2, sx: 302, sy:   0},
        TwoHollowRedSquare:       {index: 2, sx:   0, sy:  95},
        TwoHollowGreenSquare:     {index: 2, sx: 151, sy:  95},
        TwoHollowBlueSquare:      {index: 2, sx: 302, sy:  95},
        TwoSolidRedTriangle:      {index: 2, sx:   0, sy: 189},
        TwoSolidGreenTriangle:    {index: 2, sx: 151, sy: 189},
        TwoSolidBlueTriangle:     {index: 2, sx: 302, sy: 189},
        TwoShadedRedTriangle:     {index: 2, sx:   0, sy: 284},
        TwoShadedGreenTriangle:   {index: 2, sx: 151, sy: 284},
        TwoShadedBlueTriangle:    {index: 2, sx: 302, sy: 284},
        TwoHollowRedTriangle:     {index: 2, sx:   0, sy: 379},
        TwoHollowGreenTriangle:   {index: 2, sx: 151, sy: 379},
        TwoHollowBlueTriangle:    {index: 2, sx: 302, sy: 379},
        OneSolidRedCircle:        {index: 2, sx:   0, sy: 473},
        OneSolidGreenCircle:      {index: 2, sx: 151, sy: 473},
        OneSolidBlueCircle:       {index: 2, sx: 303, sy: 473},
        OneShadedRedCircle:       {index: 2, sx:   0, sy: 568},
        OneShadedGreenCircle:     {index: 2, sx: 151, sy: 568},
        OneShadedBlueCircle:      {index: 2, sx: 302, sy: 568},
        ThreeSolidRedTriangle:    {index: 3, sx:   0, sy:   0},
        ThreeSolidGreenTriangle:  {index: 3, sx: 151, sy:   0},
        ThreeSolidBlueTriangle:   {index: 3, sx: 302, sy:   0},
        ThreeShadedRedTriangle:   {index: 3, sx:   0, sy:  95},
        ThreeShadedGreenTriangle: {index: 3, sx: 151, sy:  95},
        ThreeShadedBlueTriangle:  {index: 3, sx: 302, sy:  95},
        ThreeHollowRedTriangle:   {index: 3, sx:   0, sy: 189},
        ThreeHollowGreenTriangle: {index: 3, sx: 151, sy: 189},
        ThreeHollowBlueTriangle:  {index: 3, sx: 302, sy: 189},
        TwoSolidRedCircle:        {index: 3, sx:   0, sy: 284},
        TwoSolidGreenCircle:      {index: 3, sx: 151, sy: 284},
        TwoSolidBlueCircle:       {index: 3, sx: 302, sy: 284},
        TwoShadedRedCircle:       {index: 3, sx:   0, sy: 379},
        TwoShadedGreenCircle:     {index: 3, sx: 151, sy: 379},
        TwoShadedBlueCircle:      {index: 3, sx: 302, sy: 379},
        TwoHollowRedCircle:       {index: 3, sx:   0, sy: 473},
        TwoHollowGreenCircle:     {index: 3, sx: 151, sy: 473},
        TwoHollowBlueCircle:      {index: 3, sx: 303, sy: 473},
        TwoSolidRedSquare:        {index: 3, sx:   0, sy: 568},
        TwoSolidGreenSquare:      {index: 3, sx: 151, sy: 568},
        TwoSolidBlueSquare:       {index: 3, sx: 302, sy: 568},
        ThreeSolidRedCircle:      {index: 4, sx:   0, sy:   0},
        ThreeSolidGreenCircle:    {index: 4, sx: 151, sy:   0},
        ThreeSolidBlueCircle:     {index: 4, sx: 302, sy:   0},
        ThreeShadedRedCircle:     {index: 4, sx:   0, sy:  95},
        ThreeShadedGreenCircle:   {index: 4, sx: 151, sy:  95},
        ThreeShadedBlueCircle:    {index: 4, sx: 302, sy:  95},
        ThreeHollowRedCircle:     {index: 4, sx:   0, sy: 189},
        ThreeHollowGreenCircle:   {index: 4, sx: 151, sy: 189},
        ThreeHollowBlueCircle:    {index: 4, sx: 302, sy: 189},
        ThreeSolidRedSquare:      {index: 4, sx:   0, sy: 284},
        ThreeSolidGreenSquare:    {index: 4, sx: 151, sy: 284},
        ThreeSolidBlueSquare:     {index: 4, sx: 302, sy: 284},
        ThreeShadedRedSquare:     {index: 4, sx:   0, sy: 379},
        ThreeShadedGreenSquare:   {index: 4, sx: 151, sy: 379},
        ThreeShadedBlueSquare:    {index: 4, sx: 302, sy: 379},
        ThreeHollowRedSquare:     {index: 4, sx:   0, sy: 473},
        ThreeHollowGreenSquare:   {index: 4, sx: 151, sy: 473},
        ThreeHollowBlueSquare:    {index: 4, sx: 303, sy: 473}
    };

    for (i = 1; i < 5; i++) {
        key = "cards" + i;
        this[key] = document.getElementById(key);
        this[key].className = "hidden";
    }
}

function main () {
    global.deck = new global.classes.deck();
    global.points = 0;
    global.table = new global.classes.table();
    global.ui = new global.classes.ui();

    global.table.refresh();
}
