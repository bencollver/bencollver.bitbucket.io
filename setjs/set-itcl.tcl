package require Itcl
package require Tk

itcl::class card {
    public variable num
    public variable fill
    public variable hue
    public variable shape
    public variable sel

    constructor {num fill hue shape} {
        configure -num $num -fill $fill -hue $hue -shape $shape -sel false
    }

    method clear {} {
        configure -sel false
    }

    method compareProp {c2 c3 name} {
        set c1 $this
        set retval undefined
        set uniq [dict create     \
            [$c1 getProp $name] 1 \
            [$c2 getProp $name] 1 \
            [$c3 getProp $name] 1 \
        ]
        set retval [dict keys $uniq]
        return $retval
    }

    method getName {} {
        set retval "$num$fill$hue$shape"
        return $retval
    }

    method getProp {name} {
        set retval [set $name]
        return $retval
    }

    method isSelected {} {
        set retval $sel
        return $retval
    }

    method select {} {
        configure -sel true
    }

    method validateProp {c2 c3 name} {
        set len undefined
        set retval undefined
        set uniq [compareProp $c2 $c3 $name]
        set len [llength $uniq]
        set retval [expr {$len == 1 || $len == 3}]
        return $retval
    }

    method validateSet {c2 c3} {
        set c1 $this
        set retval [expr {
            ![$c1 validateProp $c2 $c3 "fill"]  ? "fill"  :
            ![$c1 validateProp $c2 $c3 "hue"]   ? "hue"   :
            ![$c1 validateProp $c2 $c3 "num"]   ? "num"   :
            ![$c1 validateProp $c2 $c3 "shape"] ? "shape" :
                                                  "valid"
        }]
        return $retval
    }
}

itcl::class deck {
    public variable cards

    constructor {} {
        set cards [list]
        set fill undefined
        set hue undefined
        set num undefined
        set shape undefined

        # create cards
        foreach num [list One Two Three] {
            foreach fill [list Hollow Shaded Solid] {
                foreach hue [list Blue Green Red] {
                    foreach shape [list Circle Square Triangle] {
                        lappend cards [itcl::code                \
                            [card o#auto $num $fill $hue $shape]]
                    }
                }
            }
        }
        configure -cards $cards
        shuffle
    }

    method deal {} {
        set card [lindex $cards end]
        set cards [lreplace $cards end end]
        configure -cards $cards
        return $card
    }

    method getLength {} {
        set retval [llength $cards]
        return $retval
    }

    method shuffle {} {
        set i undefined
        set index undefined
        set temp undefined
        for {set i 0} {$i < [llength $cards]} {incr i} {
            set index [expr {int(floor(rand() * ([llength $cards] - 1)))}]
            set temp [lindex $cards $i]
            lset cards $i [lindex $cards $index]
            lset cards $index $temp
        }
        configure -cards $cards
    }
}

itcl::class table {
    public variable cards
    public variable min
    public variable spots
    public variable waste

    constructor {} {
        global deck
        set cards [list]
        set i undefined
        set min 12
        for {set i 0} {$i < $min} {incr i} {
            lappend cards [$deck deal]
        }

        configure -cards $cards -min $min -spots 15 -waste [list]
    }

    method clearSelection {} {
        global ui
        set card undefined
        set i undefined
        for {set i 0} {$i < [llength $cards]} {incr i} {
            set card [lindex $cards $i]
            if {[$card isSelected]} {
                $card clear
                $ui drawCard $i $card
            }
        }
    }

    method getLength {} {
        set retval [llength $cards]
        return $retval
    }

    method getWasteLength {} {
        set retval [llength $waste]
        return $retval
    }

    method getSelection {} {
        set card undefined
        set i undefined
        set retval [list]
        for {set i 0} {$i < [llength $cards]} {incr i} {
            set card [lindex $cards $i]
            if {[$card isSelected]} {
                lappend retval $card
            }
        }
        return $retval
    }

    method hasSet {} {
        set a undefined
        set b undefined
        set c undefined
        set c1 undefined
        set c2 undefined
        set c3 undefined
        for {set a 0} {$a < [llength $cards]} {incr a} {
            set c1 [lindex $cards $a]
            for {set b [expr $a + 1]} {$b < [llength $cards]} {incr b} {
                set c2 [lindex $cards $b]
                for {set c [expr $b + 1]} {$c < [llength $cards]} {incr c} {
                    set c3 [lindex $cards $c]
                    if {[$c1 validateSet $c2 $c3] eq "valid"} {
                        return true
                    }
                }
            }
        }
        return false
    }

    method more {} {
        global deck \
            ui
        set i undefined
        if {[llength $cards] > $min} {
            $ui print "Enough cards already."
        } elseif {[$deck getLength] == 0} {
            $ui print "No more cards."
        } elseif {[hasSet]} {
            $ui print "Set exists."
        } else {
            for {set i 0} {$i < 3} {incr i} {
                if {[$deck getLength] > 0} {
                    lappend cards [$deck deal]
                }
            }
            configure -cards $cards
            refresh
        }
    }

    method refresh {} {
        global ui
        set i undefined
        for {set i 0} {$i < $spots} {incr i} {
            if {$i < [llength $cards]} {
                $ui drawCard $i [lindex $cards $i]
            } else {
                $ui drawCard $i "null"
            }
        }
        $ui score
    }

    method replaceSelection {} {
        global deck
        set card undefined
        set i undefined
        for {set i 0} {$i < [llength $cards]} {incr i} {
            set card [lindex $cards $i]
            while {[$card isSelected]} {
                lappend waste $card
                if {[llength $cards] <= $min && [$deck getLength] > 0} {
                    lset cards $i [$deck deal]
                } else {
                    set cards [lreplace $cards $i $i]
                }
                set card [lindex $cards $i]
            }
        }
        configure -cards $cards -waste $waste
    }

    method select {index} {
        global points \
            ui
        set card undefined
        set c1 undefined
        set c2 undefined
        set c3 undefined
        set result "ok"
        if {$index < [getLength]} {
            set card [lindex $cards $index]
            $card select
            $ui drawCard $index $card
            set sel [getSelection]
            if {[llength $sel] == 3} {
                lassign $sel c1 c2 c3
                set result [$c1 validateSet $c2 $c3]
            }
        }
        switch $result {
        ok {
            # incomplete set, continue
        }
        valid {
            $ui print "Correct, +1 point"
            incr points
            replaceSelection
            refresh
        }
        default {
            $ui print "Wrong $result, -1 point"
            incr points -1
            clearSelection
            refresh
        }
        }
    }
}

itcl::class ui {
    public variable w
    public variable cards
    public variable images
    public variable sprites

    constructor {} {
        global points \
            table     \
            ui
        set font "Helvetica 10"
        set i undefined
        set img undefined
        set name undefined
        set w .w

        frame $w -height 320 -width 800
        frame $w.board -height 279 -width 750
        for {set i 0} {$i < 15} {incr i} {
            set name $w.board.c$i
            label $name -background white -borderwidth 5
            bind $name <ButtonRelease-1> "$table select $i"
        }
        frame $w.right -height 320 -width 50
        frame $w.right.score -height 150 -width 50
        frame $w.right.controls -height 150 -width 50

        label $w.right.score.pointsLabel -borderwidth 1 -font $font \
            -relief solid -text points -width 6
        label $w.right.score.points -borderwidth 1 -font $font \
            -relief solid -text 0 -width 6
        label $w.right.score.deckLabel -borderwidth 1 -font $font \
            -relief solid -text deck -width 6
        label $w.right.score.deck -borderwidth 1 -font $font \
            -relief solid -text 0 -width 6
        label $w.right.score.wasteLabel -borderwidth 1 -font $font \
            -relief solid -text waste -width 6
        label $w.right.score.waste -borderwidth 1 -font $font \
            -relief solid -text 0 -width 6

        button $w.right.controls.deal -command "$table more" -font $font \
            -text deal
        button $w.right.controls.about -command "$this about" \
            -font $font -text about

        grid $w.board.c0 -row 0 -column 0
        grid $w.board.c1 -row 0 -column 1
        grid $w.board.c2 -row 0 -column 2
        grid $w.board.c3 -row 0 -column 3
        grid $w.board.c12 -row 0 -column 4
        grid $w.board.c4 -row 1 -column 0
        grid $w.board.c5 -row 1 -column 1
        grid $w.board.c6 -row 1 -column 2
        grid $w.board.c7 -row 1 -column 3
        grid $w.board.c13 -row 1 -column 4
        grid $w.board.c8 -row 2 -column 0
        grid $w.board.c9 -row 2 -column 1
        grid $w.board.c10 -row 2 -column 2
        grid $w.board.c11 -row 2 -column 3
        grid $w.board.c14 -row 2 -column 4
        pack $w.board -side left
        grid $w.right.score.pointsLabel -row 0 -column 0
        grid $w.right.score.points -row 0 -column 1
        grid $w.right.score.deckLabel -row 1 -column 0
        grid $w.right.score.deck -row 1 -column 1
        grid $w.right.score.wasteLabel -row 2 -column 0
        grid $w.right.score.waste -row 2 -column 1
        pack $w.right.score -pady 40 -side top
        pack $w.right.controls.deal -side top
        pack $w.right.controls.about -side top
        pack $w.right.controls -side top
        pack $w.right -side left
        pack $w

        set cards [dict create]
        dict set cards 1 [image create photo -file setcards1.gif]
        dict set cards 2 [image create photo -file setcards2.gif]
        dict set cards 3 [image create photo -file setcards3.gif]
        dict set cards 4 [image create photo -file setcards4.gif]

        set images [dict create]
        for {set i 0} {$i < 15} {incr i} {
            set img [image create photo -height 95 -width 152]
            dict set images $i $img
            $w.board.c$i configure -image $img
        }

        set sprites [dict create                      \
            OneHollowRedCircle       [list 1   0   0] \
            OneHollowGreenCircle     [list 1 151   0] \
            OneHollowBlueCircle      [list 1 303   0] \
            OneSolidRedSquare        [list 1   0  95] \
            OneSolidGreenSquare      [list 1 151  95] \
            OneSolidBlueSquare       [list 1 302  95] \
            OneShadedRedSquare       [list 1   0 189] \
            OneShadedGreenSquare     [list 1 152 189] \
            OneShadedBlueSquare      [list 1 302 189] \
            OneHollowRedSquare       [list 1   0 284] \
            OneHollowGreenSquare     [list 1 151 284] \
            OneHollowBlueSquare      [list 1 302 284] \
            OneSolidRedTriangle      [list 1   0 379] \
            OneSolidGreenTriangle    [list 1 151 379] \
            OneSolidBlueTriangle     [list 1 302 379] \
            OneShadedRedTriangle     [list 1   0 473] \
            OneShadedGreenTriangle   [list 1 151 473] \
            OneShadedBlueTriangle    [list 1 303 473] \
            OneHollowRedTriangle     [list 1   0 568] \
            OneHollowGreenTriangle   [list 1 151 568] \
            OneHollowBlueTriangle    [list 1 302 568] \
            TwoShadedRedSquare       [list 2   0   0] \
            TwoShadedGreenSquare     [list 2 151   0] \
            TwoShadedBlueSquare      [list 2 302   0] \
            TwoHollowRedSquare       [list 2   0  95] \
            TwoHollowGreenSquare     [list 2 151  95] \
            TwoHollowBlueSquare      [list 2 302  95] \
            TwoSolidRedTriangle      [list 2   0 189] \
            TwoSolidGreenTriangle    [list 2 151 189] \
            TwoSolidBlueTriangle     [list 2 302 189] \
            TwoShadedRedTriangle     [list 2   0 284] \
            TwoShadedGreenTriangle   [list 2 151 284] \
            TwoShadedBlueTriangle    [list 2 302 284] \
            TwoHollowRedTriangle     [list 2   0 379] \
            TwoHollowGreenTriangle   [list 2 151 379] \
            TwoHollowBlueTriangle    [list 2 302 379] \
            OneSolidRedCircle        [list 2   0 473] \
            OneSolidGreenCircle      [list 2 151 473] \
            OneSolidBlueCircle       [list 2 303 473] \
            OneShadedRedCircle       [list 2   0 568] \
            OneShadedGreenCircle     [list 2 151 568] \
            OneShadedBlueCircle      [list 2 302 568] \
            ThreeSolidRedTriangle    [list 3   0   0] \
            ThreeSolidGreenTriangle  [list 3 151   0] \
            ThreeSolidBlueTriangle   [list 3 302   0] \
            ThreeShadedRedTriangle   [list 3   0  95] \
            ThreeShadedGreenTriangle [list 3 151  95] \
            ThreeShadedBlueTriangle  [list 3 302  95] \
            ThreeHollowRedTriangle   [list 3   0 189] \
            ThreeHollowGreenTriangle [list 3 151 189] \
            ThreeHollowBlueTriangle  [list 3 302 189] \
            TwoSolidRedCircle        [list 3   0 284] \
            TwoSolidGreenCircle      [list 3 151 284] \
            TwoSolidBlueCircle       [list 3 302 284] \
            TwoShadedRedCircle       [list 3   0 379] \
            TwoShadedGreenCircle     [list 3 151 379] \
            TwoShadedBlueCircle      [list 3 302 379] \
            TwoHollowRedCircle       [list 3   0 473] \
            TwoHollowGreenCircle     [list 3 151 473] \
            TwoHollowBlueCircle      [list 3 303 473] \
            TwoSolidRedSquare        [list 3   0 568] \
            TwoSolidGreenSquare      [list 3 151 568] \
            TwoSolidBlueSquare       [list 3 302 568] \
            ThreeSolidRedCircle      [list 4   0   0] \
            ThreeSolidGreenCircle    [list 4 151   0] \
            ThreeSolidBlueCircle     [list 4 302   0] \
            ThreeShadedRedCircle     [list 4   0  95] \
            ThreeShadedGreenCircle   [list 4 151  95] \
            ThreeShadedBlueCircle    [list 4 302  95] \
            ThreeHollowRedCircle     [list 4   0 189] \
            ThreeHollowGreenCircle   [list 4 151 189] \
            ThreeHollowBlueCircle    [list 4 302 189] \
            ThreeSolidRedSquare      [list 4   0 284] \
            ThreeSolidGreenSquare    [list 4 151 284] \
            ThreeSolidBlueSquare     [list 4 302 284] \
            ThreeShadedRedSquare     [list 4   0 379] \
            ThreeShadedGreenSquare   [list 4 151 379] \
            ThreeShadedBlueSquare    [list 4 302 379] \
            ThreeHollowRedSquare     [list 4   0 473] \
            ThreeHollowGreenSquare   [list 4 151 473] \
            ThreeHollowBlueSquare    [list 4 303 473] \
        ]

        configure -cards $cards -images $images -sprites $sprites -w $w
    }

    method about {} {
        print "About set solitaire version 6.

A solitaire version of the game of set.  Written by Ben Collver and
released into the public domain.

Read the rules at the following location.

http://web.archive.org/web/20080527145651/http://www.madras.fife.sch.uk/maths/games/set/index.html
"
    }

    method drawCard {spot card} {
        set frame $w.board.c$spot
        set img [dict get $images $spot]
        set sprite undefined

        if {$card eq "null"} {
            $img blank
            $frame configure -background white
        } else {
            set sprite [dict get $sprites [$card getName]]
            draw $img {*}$sprite
            if {[$card isSelected]} {
                $frame configure -background purple
            } else {
                $frame configure -background white
            }
        }
    }

    method draw {img num x y} {
        set src [dict get $cards $num]
        set x2 [expr {$x + 152}]
        set y2 [expr {$y + 95}]
        $img copy $src -from $x $y $x2 $y2
    }

    method print {str} {
        tk_messageBox -icon info -message $str -type ok
    }

    method score {} {
        global deck \
            points  \
            table
        $w.right.score.points configure -text $points
        $w.right.score.deck configure -text [$deck getLength]
        $w.right.score.waste configure -text [$table getWasteLength]
    }
}

proc main {} {
    global deck \
        points  \
        table   \
        ui

    set deck [deck o#auto]
    set points 0
    set table [table o#auto]
    set ui [ui o#auto]

    $table refresh
}

main
