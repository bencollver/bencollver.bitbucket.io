// Set solitaire game for DOS
// ==========================
// Run: dojs -r setdos.js
//
// Controls:
// * To quit, press Esc
// * To select a card, click it or type its label
// * To deal more cards, click "More cards" or press M
// * To acknowledge an alert, press Enter or Space
//
// Tested on DoJS 1.2.0 running in FreeDOS on VirtualBox

Include("p5");

// make global variables explicit
global = (function () {
    return this;
}).call(null);

// use global.classes as a "namespace"
if (typeof global.classes == "undefined") {
    global.classes = {};
}

global.classes.card = function (num, fill, hue, shape) {
    this.clear = function () {
        this.sel = false;
    };

    this.compareProp = function (c2, c3, name) {
        var c1 = this,
            key,
            retval = [],
            uniq = {};
        uniq[c1.getProp(name)] = 1;
        uniq[c2.getProp(name)] = 1;
        uniq[c3.getProp(name)] = 1;
        for (key in uniq) {
            retval.push(key);
        }
        return retval;
    };

    this.getName = function () {
        var retval = this.num + this.fill + this.hue + this.shape;
        return retval;
    };

    this.getProp = function (name) {
        var retval = this[name];
        return retval;
    };

    this.isSelected = function () {
        var retval = this.sel;
        return retval;
    };

    this.select = function () {
        this.sel = true;
    };

    this.validateProp = function (c2, c3, name) {
        var len,
            retval,
            uniq;
        uniq = this.compareProp(c2, c3, name);
        len = uniq.length;
        retval = (len == 1 || len == 3);
        return retval;
    };

    this.validateSet = function (c2, c3) {
        var c1 = this,
            retval;
        retval = (
            !c1.validateProp(c2, c3, "fill")  ? "fill"  :
            !c1.validateProp(c2, c3, "hue")   ? "hue"   :
            !c1.validateProp(c2, c3, "num")   ? "num"   :
            !c1.validateProp(c2, c3, "shape") ? "shape" :
                                                "valid"
        );
        return retval;
    };

    this.num = num;
    this.fill = fill;
    this.hue = hue;
    this.shape = shape;
    this.clear();
}

global.classes.deck = function () {
    var f,
        fills,
        h,
        hues,
        n,
        nums,
        s,
        shapes;

    this.deal = function () {
        var retval = this.cards.shift();
        return retval;
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.shuffle = function () {
        var i,
            index,
            temp;
        for (i = 0; i < this.cards.length; i++) {
            index = parseInt(Math.floor(Math.random() *
                (this.cards.length - 1)), 10);
            temp = this.cards[i];
            this.cards[i] = this.cards[index];
            this.cards[index] = temp;
        }
    };

    this.cards = [];

    // create cards
    nums = ["One", "Two", "Three"];
    fills = ["Hollow", "Shaded", "Solid"];
    hues = ["Blue", "Green", "Red"];
    shapes = ["Circle", "Square", "Triangle"];
    for (n = 0; n < nums.length; n++) {
        for (f = 0; f < fills.length; f++) {
            for (h = 0; h < hues.length; h++) {
                for (s = 0; s < shapes.length; s++) {
                    this.cards.push(new global.classes.card(
                        nums[n],
                        fills[f],
                        hues[h],
                        shapes[s]
                    ));
                }
            }
        }
    }

    this.shuffle();
}

global.classes.table = function () {
    var i;

    this.clearSelection = function () {
        var card,
            i;
        for (i = 0; i < this.cards.length; i++) {
            card = this.cards[i];
            if (card.isSelected()) {
                card.clear();
                global.ui.drawCard(i, card);
            }
        }
    };

    this.getLength = function () {
        var retval = this.cards.length;
        return retval;
    };

    this.getSelection = function () {
        var i,
            retval = [];
        for (i = 0; i < this.cards.length; i++) {
            if (this.cards[i].isSelected()) {
                retval.push(this.cards[i]);
            }
        }
        return retval;
    };

    this.getWasteLength = function () {
        var retval = this.waste.length;
        return retval;
    };

    this.hasSet = function () {
        var a,
            b,
            c,
            c1,
            c2,
            c3;
        for (a = 0; a < this.cards.length; a++) {
            c1 = this.cards[a];
            for (b = a + 1; b < this.cards.length; b++) {
                c2 = this.cards[b];
                for (c = b + 1; c < this.cards.length; c++) {
                    c3 = this.cards[c];
                    if (c1.validateSet(c2, c3) == "valid") {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    this.more = function () {
        var i;
        if (this.cards.length > this.min) {
            global.ui.print("Enough cards already.");
        } else if (global.deck.getLength() === 0) {
            global.ui.print("No more cards.");
        } else if (this.hasSet()) {
            global.ui.print("Set exists.");
        } else {
            for (i = 0; i < 3; i++) {
                if (global.deck.getLength() > 0) {
                    this.cards.push(global.deck.deal());
                }
            }
            // this.refresh();
        }
    };

    this.refresh = function () {
        var i;
        for (i = 0; i < this.spots; i++) {
            if (i < this.cards.length) {
                global.ui.drawCard(i, this.cards[i]);
            } else {
                global.ui.drawCard(i, null);
            }
        }
        global.ui.drawAlert();
        global.ui.drawButton();
        global.ui.drawScore();
    };

    this.replaceSelection = function () {
        var i;
        for (i = 0; i < this.cards.length; i++) {
            while (typeof this.cards[i] != "undefined" &&
                this.cards[i].isSelected()
            ) {
                this.waste.push(this.cards[i]);
                if (this.cards.length <= this.min &&
                    global.deck.getLength() > 0
                ) {
                    this.cards[i] = global.deck.deal();
                } else {
                    this.cards.splice(i, 1);
                }
            }
        }
    };

    this.select = function (index) {
        var card,
            c1,
            c2,
            c3,
            result = "ok";
        if (index < this.cards.length) {
            card = this.cards[index];
            card.select();
            global.ui.drawCard(index, card);
            var sel = this.getSelection();
            if (sel.length == 3) {
                c1 = sel[0];
                c2 = sel[1];
                c3 = sel[2];
                result = c1.validateSet(c2, c3);
            }
        }
        return result;
    };

    this.cards = [];
    this.min = 12;
    this.spots = 15;
    this.waste = [];
    for (i = 0; i < this.min; i++) {
        this.cards.push(global.deck.deal());
    }
}

global.classes.cardUI = function () {
    this.border = 5;
    this.pad = 1;

    this.cardHeightPx = 110;
    this.cardWidthPx = 75;
    this.paddingPx = 8;
    this.shapeSizePx = 20;

    this.height = this.cardHeightPx + 2 * this.border + 2 * this.pad;
    this.width = this.cardWidthPx + 2 * this.border + 2 * this.pad;

    this.drawCardFace = function (x, y, label, card) {
        var cardNum,
            cardShape,
            cx,
            cy,
            radius,
            retval,
            x0,
            x3,
            y0,
            y3;

        push();

        // fill white rectangle to draw card into
        fill(global.palette.White);
        stroke(global.palette.White);
        strokeWeight(1);
        rectMode(CORNER);
        rect(x, y, this.width, this.height);

        // draw card outline
        x0 = x + this.paddingPx;
        y0 = y + this.paddingPx;
        x3 = x + this.cardWidthPx - this.paddingPx;
        y3 = y + this.cardHeightPx - this.paddingPx;
        radius = 10;
        fill(global.palette.Alabaster);
        if (card.isSelected()) {
            stroke(global.palette.Purple);
            strokeWeight(this.border);
        } else {
            stroke(global.palette.Black);
            strokeWeight(1);
        }
        rectMode(CORNERS);
        rect(x0, y0, x3, y3, radius);

        // draw card label
        x0 = x;
        y0 = y + this.cardHeightPx - 8;
        fill(global.palette.Black);
        text(label, x0, y0);

        pop();

        cardNum = card.getProp("num");
        cardShape = card.getProp("shape");
        cx = x + int(this.cardWidthPx / 2);
        cy = y + int(this.cardHeightPx / 2);
        switch (cardNum) {
        case "One":
            cx -= int(this.shapeSizePx / 2);
            cy -= int(this.shapeSizePx / 2);
            this.drawShape(cx, cy, card);
            break;
        case "Two":
            cx -= int(this.shapeSizePx / 2);
            cy -= (this.shapeSizePx + this.paddingPx);
            this.drawShape(cx, cy, card);
            cy += (this.shapeSizePx + (2 * this.paddingPx));
            this.drawShape(cx, cy, card);
            break;
        case "Three":
            cx -= int(this.shapeSizePx / 2);
            cy -= int(this.shapeSizePx / 2);
            this.drawShape(cx, cy, card);
            cy -= (this.shapeSizePx + this.paddingPx);
            this.drawShape(cx, cy, card);
            cy += (2 * (this.shapeSizePx + this.paddingPx));
            this.drawShape(cx, cy, card);
            break;
        }
        return;
    };

    this.drawShape = function (x, y, card) {
        var cardFill,
            cardHue,
            cardShape,
            fillHue,
            strokeHue;

        cardFill = card.getProp("fill");
        cardHue = card.getProp("hue");
        cardShape = card.getProp("shape");

        switch (cardFill) {
        case "Hollow":
            fillHue = global.palette.Alabaster;
            strokeHue = global.palette[cardHue];
            break;
        case "Shaded":
            fillHue = global.palette["pale" + cardHue];
            strokeHue = global.palette[cardHue];
            break;
        case "Solid":
            fillHue = global.palette[cardHue];
            strokeHue = global.palette[cardHue];
            break;
        }
        fill(fillHue);
        stroke(strokeHue);
        strokeWeight(2);

        switch (cardShape) {
        case "Circle":
            this.drawShapeCircle(x, y);
            break;
        case "Square":
            this.drawShapeSquare(x, y);
            break;
        case "Triangle":
            this.drawShapeTriangle(x, y);
            break;
        }

        return;
    };

    this.drawShapeCircle = function(x, y) {
        var x2,
            y2;
        x += int(0.5 * this.shapeSizePx);
        y += int(0.5 * this.shapeSizePx);
        x2 = x + this.shapeSizePx;
        y2 = y + this.shapeSizePx;
        ellipse(x, y, x2, y2);
        return;
    };

    this.drawShapeSquare = function(x, y) {
        var x2,
            y2;
        x2 = x + this.shapeSizePx;
        y2 = y + this.shapeSizePx;
        rect(x, y, x2, y2);
        return;
    };

    this.drawShapeTriangle = function(x, y) {
        var x1,
            x2,
            x3,
            y1,
            y2,
            y3;
        x1 = x;
        y1 = y + this.shapeSizePx;
        x2 = x + int(this.shapeSizePx / 2);
        y2 = y;
        x3 = x + this.shapeSizePx;
        y3 = y1;
        triangle(x1, y1, x2, y2, x3, y3);
        return;
    };

    this.getHeight = function () {
        return this.height;
    };

    this.getWidth = function () {
        return this.width;
    };
};

global.classes.ui = function () {
    var cardUI;

    this.alert = function (str) {
        this.alertText = str;
        this.state = "alert";
        return;
    };

    this.clearAlert = function () {
        if (this.state == "clear") {
            global.table.clearSelection();
        } else if (this.state == "replace") {
            global.table.replaceSelection();
        };
        this.alertText = "";
        this.state = "normal";
        return;
    };

    this.click = function (e) {
        var index;

        if (this.state == "normal") {
            if (e.x >= 500 && e.x <= 600 && e.y >= 100 && e.y <= 120) {
                // a click on the More button
                global.table.more();
            } else {
                // possibly a click on a card
                index = this.getLocationByClick(e.x, e.y);
                this.selectLocation(index);
            }
        }
        return;
    };

    this.drawAlert = function () {
        if (this.state == "normal") {
            fill(global.palette.White);
            stroke(global.palette.White);
            rect(0, 400, 640, 420);
        } else {
            fill(global.palette.Blue);
            text(this.alertText, 0, 400);
            fill(global.palette.Purple);
            text("Press Enter or Space to continue...", 0, 410);
        }
        return;
    };

    this.drawButton = function () {
        fill(global.palette.Alabaster);
        stroke(global.palette.Black);
        strokeWeight(1);
        rect(500, 100, 600, 120);
        fill(global.palette.Black);
        text("More cards", 511, 107);
        line(511, 116, 517, 116);
        return;
    };

    this.drawCard = function (spot, card) {
        var loc = this.locations[spot];

        if (card !== null) {
            cardUI.drawCardFace(loc.x, loc.y, loc.label, card);
        }
        return;
    };

    this.drawScore = function () {
        fill(global.palette.Black);
        text("points: " + global.points, 500, 30);
        text("deck:   " + global.deck.getLength(), 500, 40);
        text("waste:  " + global.table.getWasteLength(), 500, 50);
        return;
    };

    this.getLocationByClick = function(x, y) {
        var i,
            l,
            retval;
        retval = null;
        for (i = 0; i < this.spots; i++) {
            l = this.locations[i];
            if (x > l.x                 &&
                x < (l.x + this.width)  &&
                y > l.y                 &&
                y < (l.y + this.height)
            ) {
                retval = i;
                break;
            }
        }
        return retval;
    };

    this.getLocationByLabel = function (name) {
        var i,
            l,
            retval;
        retval = null;
        for (i = 0; i < this.spots; i++) {
            l = this.locations[i];
            if (name == l.label) {
                retval = i;
                break;
            }
        }
        return retval;
    };

    this.input = function (str) {
        var index;

        if (this.state == "normal") {
            index = null;
            switch (str) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "A":
            case "B":
            case "C":
            case "D":
            case "E":
                index = this.getLocationByLabel(str);
                this.selectLocation(index);
                break;
            case "M":
                global.table.more();
                break;
            }
        } else {
            if (str == " ") {
                this.clearAlert();
            }
        }
        return;
    };

    this.print = function (str) {
        this.alert(str);
        return;
    };

    this.selectLocation = function (index) {
        var result;

        if (index === null) {
            result = "ok";
        } else {
            result = table.select(index);
        }
        switch (result) {
        case "ok":
            // incomplete set, continue
            break;
        case "valid":
            global.points++;
            this.print("Correct, +1 point");
            this.state = "replace";
            break;
        default:
            global.points--;
            this.print("Wrong " + result + ", -1 point");
            this.state = "clear";
            break;
        }

        return;
    };

    cardUI = new global.classes.cardUI();
    this.height = cardUI.getHeight();
    this.width = cardUI.getWidth();
    this.locations = {
        "0":  {label: "0", x: 0 * this.width, y: 0 * this.height},
        "1":  {label: "1", x: 1 * this.width, y: 0 * this.height},
        "2":  {label: "2", x: 2 * this.width, y: 0 * this.height},
        "3":  {label: "3", x: 3 * this.width, y: 0 * this.height},
        "12": {label: "C", x: 4 * this.width, y: 0 * this.height},
        "4":  {label: "4", x: 0 * this.width, y: 1 * this.height},
        "5":  {label: "5", x: 1 * this.width, y: 1 * this.height},
        "6":  {label: "6", x: 2 * this.width, y: 1 * this.height},
        "7":  {label: "7", x: 3 * this.width, y: 1 * this.height},
        "13": {label: "D", x: 4 * this.width, y: 1 * this.height},
        "8":  {label: "8", x: 0 * this.width, y: 2 * this.height},
        "9":  {label: "9", x: 1 * this.width, y: 2 * this.height},
        "10": {label: "A", x: 2 * this.width, y: 2 * this.height},
        "11": {label: "B", x: 3 * this.width, y: 2 * this.height},
        "14": {label: "E", x: 4 * this.width, y: 2 * this.height}
    };
    this.spots = 15;
    this.state = "normal";
};

global.Setup = function () {
    if (SizeX() != 640 || SizeY() != 480) {
        Println("Expected 640x480 video resolution, got " +
            SizeX() + "x" + SizeY());
        Stop();
    }

    global.lastButtons = 0;

    global.palette = {};

    global.palette.Alabaster = color(242, 240, 230);
    global.palette.Black = color(0, 0, 0);
    global.palette.Blue = color(0, 0, 127);
    global.palette.Green = color(0, 127, 0);
    global.palette.Purple = color(127, 0, 255);
    global.palette.Red = color(127, 0, 0);
    global.palette.White = color(255, 255, 255);

    global.palette.paleBlue = color(127, 127, 255);
    global.palette.paleGreen = color(127, 255, 127);
    global.palette.paleRed = color(255, 127, 127);

    createCanvas(windowWidth, windowHeight);
    ellipseMode(CORNERS);
    rectMode(CORNERS);
    SetFramerate(10);

    global.deck = new global.classes.deck();
    global.points = 0;
    global.table = new global.classes.table();
    global.ui = new global.classes.ui();
};

global.Loop = function () {
    background(EGA.WHITE);
    global.table.refresh();
};

global.Inputz = function(e) {
    Println(JSON.stringify(e));
};

global.Input = function (e) {
    var code,
        key;

    code = e.key >> 8;
    key = e.key & 0xFF;

    if (e.buttons < global.lastButtons) {
        // mouse button released
        ui.click(e);
    } else if (key == CharCode("0")) {
        ui.input("0");
    } else if (key == CharCode("1")) {
        ui.input("1");
    } else if (key == CharCode("2")) {
        ui.input("2");
    } else if (key == CharCode("3")) {
        ui.input("3");
    } else if (key == CharCode("4")) {
        ui.input("4");
    } else if (key == CharCode("5")) {
        ui.input("5");
    } else if (key == CharCode("6")) {
        ui.input("6");
    } else if (key == CharCode("7")) {
        ui.input("7");
    } else if (key == CharCode("8")) {
        ui.input("8");
    } else if (key == CharCode("9")) {
        ui.input("9");
    } else if ((key == CharCode("A")) || (key == CharCode("a"))) {
        ui.input("A");
    } else if ((key == CharCode("B")) || (key == CharCode("b"))) {
        ui.input("B");
    } else if ((key == CharCode("C")) || (key == CharCode("c"))) {
        ui.input("C");
    } else if ((key == CharCode("D")) || (key == CharCode("d"))) {
        ui.input("D");
    } else if ((key == CharCode("E")) || (key == CharCode("e"))) {
        ui.input("E");
    } else if ((key == CharCode("M")) || (key == CharCode("m"))) {
        ui.input("M");
    } else if ((code == KEY.Code.KEY_ENTER) || (key == CharCode(" "))) {
        ui.input(" ");
    }
    global.lastButtons = e.buttons;
};
